@echo off
setlocal enabledelayedexpansion
set basedir=%~dp0
set basedir=%basedir:~0,-1%

for /f "usebackq tokens=*" %%i in (`docker images ^| %basedir%\..\tools\egrep -v "REPOSITORY|<none>" ^| %basedir%\..\tools\awk "{print $1\":\"$2}"`) do (
  set filename=%%~i&& set filename=!filename::=_!&& set filename=!filename:/=_!&& echo Backup %%~i && docker save %%~i | %basedir%\..\tools\gzip -c > %basedir%\archives\!filename!.tgz
)
