@echo off
setlocal enabledelayedexpansion
set basedir=%~dp0
set basedir=%basedir:~0,-1%

for %%f in (%basedir%\archives\*.tgz) do ( 
   echo Restore %%f && %basedir%\..\tools\gzip -d -c %%f | docker load
)