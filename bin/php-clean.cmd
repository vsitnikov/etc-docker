@echo off
set cd=%CD%
docker run -ti --rm -v %cd%:/app -w /app php:7.2-fpm-alpine php %*
