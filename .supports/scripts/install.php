<?php
$rundir = __DIR__."/../..";
$os = $argv[1];
$osuser = substr($argv[2], 0, 7) == "osuser=" ? substr($argv[2], 7): "";
$basedir = substr($argv[2], 0, 7) == "osuser=" ? $argv[3]: $argv[2];
$params_list = [
  "php",
  "port",
  "www",
  "user",
  "q",
  "php_pdo-odbc",
  "sybase-freedts",
  "sybase_devart",
  "oracle",
];
$params = [];
for ($i = 3; $i < $argc; $i++) {
    $param = [];
    if (preg_match("/^--(?<key>[a-z]+)=(?<value>.*)$/", $argv[$i], $regex))
        $param[$regex['key']] = $regex['value'];
    else if (preg_match("/^-(?<key>[a-z]+)$/", $argv[$i], $regex))
        $param[$regex['key']] = true;
    else if (preg_match("/^--(?<key>[a-z]+)$/", $argv[$i], $regex))
      $param[$regex['key']] = $argv[++$i];
    else
      $params[] = $argv[$i];
    if (sizeof($param) && !sizeof(array_diff_key($param, array_flip($params_list))))
      extract($param);
}

$images = array_values(array_diff(scandir("{$rundir}/images"), [".", ".."]));
for ($i = 0; $i < sizeof($images); $i++)
    if (substr($images[$i], 0, 3) == "php")
        $php_list[substr($images[$i], strpos($images[$i], "-") !== false ? strpos($images[$i], "-") + 1 : 0)] = ["id" => $images[$i]];

# Setup php image
while (!in_array(trim($php), array_keys($php_list)) && !$q) {
  print "Введите образ для php (".implode(",", array_keys($php_list)).") [".array_keys($php_list)[0]."]: ";
  $php = trim(fgets(STDIN));
  if(trim($php) == "")
    $php = array_keys($php_list)[0];
}
if(!in_array(trim($php), array_keys($php_list)))
  $php = array_keys($php_list)[0];
$php = $php_list[$php]['id'];

# Setup nginx port
while (!preg_match('/^\d+$/', $port) && !$q) {
  print "Введите порт для nginx [7000]: ";
  $port = trim(fgets(STDIN));
  if (trim($port) == "")
    $port = 7000;
}
if(!preg_match('/^\d+$/', $port))
  $port = 7000;

# Setup document_root
while (trim($www) == "" && !$q) {
  print "Введите каталог document_root [./www]: ";
  $www = trim(fgets(STDIN));
  if(trim($www) == "")
    $www = "./www";
}
if(trim($www) == "")
  $www = "./www";

# Setup user
while (trim($user) == "" && !$q) {
  print "Введите пользователя [php]: ";
  $user = trim(fgets(STDIN));
  if(trim($user) == "")
    $user = "php";
}
if(trim($user) == "")
  $user = "php";

# Setup Logs
$logs = "{$basedir}/logs";

$compose = file_get_contents("{$rundir}/images/composes/docker-compose-dev.yml");
$compose = str_replace(["{{PORT}}", "{{DOCUMENT_ROOT}}", "{{PHP}}", "{{LOGS_DIR}}"], [$port, $www, $php, $logs], $compose);
file_put_contents("{$rundir}/images/docker-compose.yml", $compose);
$docker_run = ($os == "windows") ? "set install_user={$user}\r\nset install_group={$user}\r\nset install_uid=1000\r\nset install_gid=1000\r\n" : "export install_user={$user}\nexport install_group={$user}\nexport install_uid=0\nexport install_gid=0\n";
$docker_run .= "docker-compose build --force-rm";
file_put_contents("{$rundir}/images/post_install.cmd", $docker_run);
if ($osuser != "") {
    chmod("{$rundir}/images/docker-compose.yml", 0666);
    chmod("{$rundir}/images/post_install.cmd", 0777);
}
