@echo off
setlocal enabledelayedexpansion

set interactive=1
echo %cmdcmdline% | find /i "%~0" >nul
if not errorlevel 1 set interactive=0

set cd=%CD%
set basedir=%~dp0
set basedir=%basedir:~0,-1%
cd %basedir%\images
docker-compose up -d %1
cd %cd%

if _%interactive%_==_0_ pause
