@echo off
setlocal enabledelayedexpansion
set cd=%CD%
set basedir=%~dp0
set basedir=%basedir:~0,-1%
set os=windows
set interactive=1
echo %cmdcmdline% | find /i "%~0" >nul
if not errorlevel 1 set interactive=0

docker pull php:7.2-fpm-alpine 2>nul
for /f "usebackq tokens=*" %%i in (`docker images ^| %basedir%\tools\grep -P "php\s+7.2-fpm-alpine\s+"`) do set php_alpine=%%~i
if "%php_alpine%"=="" (
  echo Offline mode
  %basedir%\tools\gzip -d -c %basedir%\.supports\php_7.2-fpm-alpine.tgz | docker load
)
set os=windows

copy /Y %basedir%\.supports\scripts\delete.cmd %basedir%
call %basedir%\delete.cmd 2>nul
docker run -ti --rm -v %basedir%:/app -w /app php:7.2-fpm-alpine php /app/.supports/scripts/install.php %os% "%basedir%" %* && cd %basedir%\images && call post_install.cmd && del /Q post_install.cmd && cd %cd% && copy /Y %basedir%\.supports\scripts\*.cmd %basedir%

cscript //Nologo "%basedir%\tools\unsetpath.vbs" "%basedir%\bin"
cscript //Nologo "%basedir%\tools\setpath_first.vbs" "%basedir%\bin"

if _%interactive%_==_0_ pause
